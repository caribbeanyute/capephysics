*version 9.1 226792401
u 114
U? 2
R? 4
V? 5
@libraries
@analysis
.TRAN 1 0 0 0
+0 10ms
+1 1s
.OP 0 
@targets
@attributes
@translators
a 0 u 13 0 0 0 hln 100 PCBOARDS=PCB
a 0 u 13 0 0 0 hln 100 PSPICE=PSPICE
a 0 u 13 0 0 0 hln 100 XILINX=XILINX
@setup
unconnectedPins 0
connectViaLabel 0
connectViaLocalLabels 0
NoStim4ExtIFPortsWarnings 1
AutoGenStim4ExtIFPorts 1
@index
pageloc 1 0 3524 
@status
n 0 120:00:04:13:26:24;1578162384 e 
s 2832 120:00:04:13:26:27;1578162387 e 
*page 1 0 970 720 iA
@ports
port 38 GND_EARTH 480 200 h
port 9 GND_EARTH 330 290 h
port 37 GND_EARTH 60 300 h
port 111 GND_EARTH 210 150 h
@parts
part 55 VDC 430 60 h
a 0 sp 0 0 22 37 hln 100 PART=VDC
a 1 u 13 0 -11 18 hcn 100 DC=12V
a 0 x 0:13 0 0 0 hln 100 PKGREF=+Vdc
a 1 xp 9 0 24 7 hcn 100 REFDES=+Vdc
part 65 VDC 420 280 h
a 0 sp 0 0 22 37 hln 100 PART=VDC
a 1 u 13 0 -11 18 hcn 100 DC=12V
a 0 x 0:13 0 0 0 hln 100 PKGREF=-Vdc
a 1 xp 9 0 24 7 hcn 100 REFDES=-Vdc
part 3 r 110 210 h
a 0 sp 0 0 0 10 hlb 100 PART=r
a 0 s 0:13 0 0 0 hln 100 PKGTYPE=RC05
a 0 s 0:13 0 0 0 hln 100 GATE=
a 0 a 0:13 0 0 0 hln 100 PKGREF=R1
a 0 ap 9 0 15 0 hln 100 REFDES=R1
part 5 r 330 250 v
a 0 sp 0 0 0 10 hlb 100 PART=r
a 0 s 0:13 0 0 0 hln 100 PKGTYPE=RC05
a 0 s 0:13 0 0 0 hln 100 GATE=
a 0 a 0:13 0 0 0 hln 100 PKGREF=R3
a 0 ap 9 0 15 0 hln 100 REFDES=R3
a 0 u 13 0 15 25 hln 100 VALUE=10k
part 2 uA741 220 150 h
a 0 sp 11 0 0 70 hcn 100 PART=uA741
a 0 s 0:13 0 0 0 hln 100 PKGTYPE=DIP8
a 0 s 0:13 0 0 0 hln 100 GATE=
a 0 a 0:13 0 0 0 hln 100 PKGREF=U1
a 0 ap 9 0 14 0 hln 100 REFDES=U1
part 8 Vsin 60 210 h
a 1 u 0 0 0 0 hcn 100 DC=0
a 1 u 0 0 0 0 hcn 100 AC=0.1
a 1 u 0 0 0 0 hcn 100 VOFF=0
a 1 u 0 0 0 0 hcn 100 VAMPL=0.1
a 0 x 0:13 0 0 0 hln 100 PKGREF=Vsin
a 1 xp 9 0 20 10 hcn 100 REFDES=Vsin
a 1 u 0 0 0 0 hcn 100 FREQ=100
part 4 r 180 90 h
a 0 sp 0 0 0 10 hlb 100 PART=r
a 0 s 0:13 0 0 0 hln 100 PKGTYPE=RC05
a 0 s 0:13 0 0 0 hln 100 GATE=
a 0 a 0:13 0 0 0 hln 100 PKGREF=R2
a 0 ap 9 0 15 0 hln 100 REFDES=R2
a 0 u 13 0 15 25 hln 100 VALUE=100k
part 1 titleblk 970 720 h
a 1 s 13 0 350 10 hcn 100 PAGESIZE=A
a 1 s 13 0 180 60 hcn 100 PAGETITLE=
a 1 s 13 0 300 95 hrn 100 PAGENO=1
a 1 s 13 0 340 95 hrn 100 PAGECOUNT=1
@conn
w 57
a 0 up 0:33 0 0 0 hln 100 V=
s 430 100 430 200 64
a 0 up 33 0 432 150 hlt 100 V=
s 480 200 430 200 62
s 420 200 420 280 88
s 420 200 430 200 89
w 36
s 330 290 330 250 96
w 40
s 60 300 60 250 98
w 77
a 0 sr 0 0 0 0 hln 100 LABEL=Vin
a 0 up 0:33 0 0 0 hln 100 V=
s 110 210 60 210 100
a 0 sr 3 0 95 208 hcn 100 LABEL=Vin
a 0 up 33 0 95 209 hct 100 V=
w 59
a 0 up 0:33 0 0 0 hln 100 V=
s 430 60 260 60 58
a 0 up 33 0 345 59 hct 100 V=
s 260 60 260 140 60
w 43
a 0 up 0:33 0 0 0 hln 100 V=
s 420 320 260 320 41
a 0 up 33 0 340 319 hct 100 V=
s 260 320 260 200 42
w 15
a 0 up 0:33 0 0 0 hln 100 V=
s 180 90 180 190 21
a 0 up 33 0 182 140 hlt 100 V=
s 180 210 150 210 27
s 180 190 180 210 109
s 220 190 180 190 24
w 103
a 0 sr 0:3 0 315 188 hcn 100 LABEL=Vout
a 0 up 0:33 0 0 0 hln 100 V=
s 300 170 330 170 32
a 0 sr 3 0 315 168 hcn 100 LABEL=Vout
a 0 up 33 0 315 169 hct 100 V=
s 220 90 330 90 28
s 330 90 330 170 30
s 330 170 330 210 84
w 113
a 0 up 0:33 0 0 0 hln 100 V=
s 210 150 220 150 112
a 0 up 33 0 215 149 hct 100 V=
@junction
j 180 90
+ p 4 1
+ w 15
j 220 90
+ p 4 2
+ w 103
j 430 100
+ p 55 -
+ w 57
j 430 60
+ p 55 +
+ w 59
j 480 200
+ s 38
+ w 57
j 430 200
+ w 57
+ w 57
j 330 290
+ s 9
+ w 36
j 330 250
+ p 5 1
+ w 36
j 60 300
+ s 37
+ w 40
j 150 210
+ p 3 2
+ w 15
j 110 210
+ p 3 1
+ w 77
j 420 320
+ p 65 -
+ w 43
j 420 280
+ p 65 +
+ w 57
j 260 140
+ p 2 V+
+ w 59
j 260 200
+ p 2 V-
+ w 43
j 300 170
+ p 2 OUT
+ w 103
j 330 210
+ p 5 2
+ w 103
j 330 170
+ w 103
+ w 103
j 220 190
+ p 2 -
+ w 15
j 180 190
+ w 15
+ w 15
j 220 150
+ p 2 +
+ w 113
j 210 150
+ s 111
+ w 113
j 60 250
+ p 8 -
+ w 40
j 60 210
+ p 8 +
+ w 77
@attributes
a 0 s 0:13 0 0 0 hln 100 PAGETITLE=
a 0 s 0:13 0 0 0 hln 100 PAGENO=1
a 0 s 0:13 0 0 0 hln 100 PAGESIZE=A
a 0 s 0:13 0 0 0 hln 100 PAGECOUNT=1
@graphics
